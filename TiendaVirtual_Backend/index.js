// Forma convencional de ES6 => estándar convencional de JS
// import express from express
// Forma sistema nativo de NodeJS
const express = require('express');

const router = express.Router();

// // Importar el módulo para MongoDB => mongoose
// const mongoose = require('mongoose');

// Importar las variables de entorno
// require('dotenv').config({path: 'var.env'});

// Importar la ruta a la conexión con la base de datos
const conectarDB = require('./config/db');

let app = express();

// app.use('/', function(req, res){
//     res.send('Hola Tripulantes')
// });


// uso de archivos tipo json en la app
app.use(express.json());

app.use(router);


// conexión a la base de datos con variable de entorno
// console.log(process.env.URL_MONGODB);
// mongoose.connect(process.env.URL_MONGODB)
//     .then(function(){console.log("Conexión Establecida con MongoDB Atlas.")})
//     .catch(function(e){console.log(e)})
// y archivo externo a la ruta principal (index)
conectarDB();

// Carpeta que contiene el Frontend
app.use(express.static('public'));

// // Esquema del modelo => es la estructura
// const productSchema = mongoose.Schema({
//     id: Number,
//     nombre: String, 
//     cantidad: Number,
//     categoria: String,
//     precio: Number
// });


// // const modelProducto = mongoose.model('Nomb_Collect', productSchema);
// const modelProducto = mongoose.model('productos', productSchema);
// // const modelProducto = mongoose.model('usuarios', productSchema);

// // CRUD => Create
// modelProducto.create(
//     {
//         id: 4,
//         nombre: "Lechuga",
//         cantidad: 646,
//         categoria: "Verduras",
//         precio: 1000
//     },
//     (error) => {
//         // console.log("Ingresó función flecha");
//         if (error) return console.log(error);
//         // console.log(error)
//         // console.log("Sale de la función flecha después del if (error)")
//     }
// );


// // CRUD => Read
// modelProducto.find((error, productos) => {
//     if(error) return console.log(error);
//     console.log(productos);
// });


// // CRUD => Update
// modelProducto.updateOne({id: 11}, {nombre: "Pollo", cantidad: 36, precio: 16000}, (error) => {
//     if (error) return console.log(error);
// });


// // CRUD => Delete
// modelProducto.deleteOne({cantidad: 646}, (error) => {
//     if(error) return console.log(error);
// });



// --------------------------------------------------
// #######  Descentralización del CRUD ##############
// #######    Rutas Respecto al CRUD   ##############
// --------------------------------------------------

// // uso de archivos tipo json en la app
// app.use(express.json());
// CORS (Cross-Origin Resource Sharing) => Mecanismos o reglas de seguridad para el control de peticiones http
const cors = require('cors');

app.use(cors());

// solicitudes al CRUD => el controlador
const crudProductos = require('./controller/controlProductos');


// Solución Temporal a los CORS
var whitelist = ['http://localhost:4000', 'http://localhost:4200']
var corsOptionsDelegate = function (req, callback) {
  var corsOptions;
  if (whitelist.indexOf(req.header('Origin')) !== -1) {
    corsOptions = { Origin: true } // reflect (enable) the requested Origin in the CORS response
  }else{
    corsOptions = { Origin: false } // disable CORS for this request
  }
  callback(null, corsOptions) // callback expects two parameters: error and options
}

// Establecer las rutas respecto al CRUD
// CRUD => Create
router.post('/apirest/', cors(corsOptionsDelegate), crudProductos.crear);
// CRUD => Read
router.get('/apirest/', cors(corsOptionsDelegate), crudProductos.obtener);
// CRUD => Update
router.put('/apirest/:id', cors(corsOptionsDelegate), crudProductos.actualizar);
// CRUD => Delete
router.delete('/apirest/:id', cors(corsOptionsDelegate), crudProductos.eliminar);

// // Establecer las rutas respecto al CRUD
// // CRUD => Create
// router.post('/', crudProductos.crear);
// // CRUD => Read
// router.get('/', crudProductos.obtener);
// // CRUD => Update
// router.put('/:id', crudProductos.actualizar);
// // CRUD => Delete
// router.delete('/:id', crudProductos.eliminar);




// router.get('/metget', function(req, res){
//     res.send("Tripulantes esta ruta es en respuesta al método GET...")
// });

// router.get('/metget', (req, res) => {
//     res.send("Tripulantes esta ruta es en respuesta al método GET...")
//     // conectarDB();
// });

// router.post('/metget', function(req, res){
//     res.send("Respuesta al método POST desde la ruta /metget")
// })

// router.post('/metpost', function(req, res){
//     res.send("Respuesta al método POST desde la ruta /metpost")
    
//     // // Conexión con la base de datos
//     // console.log("Mensaje antes de la conexión a la db.")
//     // const user = 'b21gxx';
//     // const psw = 'b21gxx123';
//     // const db = 'b21gxx';
//     // const url = `mongodb+srv://${user}:${psw}@misiontic-uis-jas.krymo.mongodb.net/${db}?retryWrites=true&w=majority`;

//     // mongoose.connect(url)
//     //     .then(function(){console.log("Conexión Establecida con MongoDB Atlas.")})
//     //     .catch(function(e){console.log(e)})


// })


// Conexión con la base de datos
// const user = 'b21gxx';
// const psw = 'b21gxx123';
// const db = 'b21gxx';
// const url = `mongodb+srv://${user}:${psw}@misiontic-uis-jas.krymo.mongodb.net/${db}?retryWrites=true&w=majority`;

// mongoose.connect(url)
//     .then(function(){console.log("Conexión Establecida con MongoDB Atlas.")})
//     .catch(function(e){console.log(e)})



// app.listen(4000);
app.listen(process.env.PORT);

console.log("La aplicación se ejecuta desde http://localhost:4000");
