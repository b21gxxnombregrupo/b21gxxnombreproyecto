// importar el modelo (squema) de producto que definimos
const modelProducto = require('../models/modelProductos');

// Exportar en diferentes variables los métodos para el CRUD

// CRUD => Create
exports.crear = async (req, res) => {

    try {
        
        let producto;

        console.log(req.body);
        producto = new modelProducto(req.body);
        // producto = new modelProducto({
        //     id: 7,
        //     nombre: "Chatas",
        //     cantidad: 46,
        //     categoria: "Carnes",
        //     precio: 23000
        // });

        await producto.save();

        res.send(producto);

    } catch (error) {
        console.log(error);
        res.status(500).send('Error al guardar producto.');
    }
}


// CRUD => Read
exports.obtener = async (req, res) => {

    try {
        
        const producto = await modelProducto.find();
        res.json(producto);

    } catch (error) {
        console.log(error);
        res.status(500).send('Error al obtener el/los producto(s).');
    }
}


// CRUD => Update
exports.actualizar = async (req, res) => {

    try {
        
        const producto = await modelProducto.findById(req.params.id);

        // console.log(producto);
        if (!producto){
            console.log(producto);
            res.status(404).json({msg: 'El producto no existe.'});
        }
        else{
            await modelProducto.findByIdAndUpdate({_id: req.params.id}, req.body);
            res.json({msg: 'Producto actualizado correctamente.'});
        }
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al actualizar el producto.');
    }
}


// CRUD => Delete
exports.eliminar = async (req, res) => {

    try {
        
        const producto = await modelProducto.findById(req.params.id);

        // console.log(producto);
        if (!producto){
            console.log(producto);
            res.status(404).json({msg: 'El producto no existe.'});
        }
        else{
            await modelProducto.findByIdAndRemove(req.params.id);
            res.json({mensaje: 'Producto eliminado correctamente.'});
        }
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al eliminar el producto.')
    }
}
