// Importar el módulo para MongoDB => mongoose
const mongoose = require('mongoose');

const productSchema = mongoose.Schema({
    id: Number,
    nombre: String, 
    cantidad: Number,
    categoria: String,
    precio: Number,
    urlimg: String
},
{
    versionKey: false,
    timestamps: true
});

// Para utilizar en archivos externos debo exportar como módulo
module.exports = mongoose.model('productos', productSchema);