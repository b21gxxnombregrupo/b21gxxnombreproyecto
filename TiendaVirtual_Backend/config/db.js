// Importar el módulo para MongoDB => mongoose
const mongoose = require('mongoose');

// Importar las variables de entorno
require('dotenv').config({path: 'var.env'});

const conexionDB = async () => {

    try {
        
        await mongoose.connect(process.env.URL_MONGODB, {});
        console.log("Conexión Establecida con MongoDB Atlas desde ./config/db.js");

    } catch (error) {
        console.log("Error en la conexión a la base de datos.")
        console.log(error);
        process.exit(1);
    }
}
// exportar para utilizar en otros archivos.
module.exports = conexionDB;